# BooruAddict

This extension will periodically load Booru images into your Chrome browser.

## How to install

1. [Download the file as ZIP](https://gitlab.com/my452/BooruAddict/-/archive/master/BooruAddict-master.zip), then extract it.
2. Go to Chrome's extensions page. chrome://extensions/
3. Enable developer mode.
4. Click 'Load Unpacked'.
5. Select and load the extension's folder.
6. Click on the puzzle piece next to Chrome's URL bar, then click the pin to pin the extension's icon.
7. Recommended: Go to chrome://extensions/, click on the extension's 'Details' page, then click 'Allow in incognito'.

## How to configure the settings

1. Go to Chrome's extensions page. chrome://extensions/
2. Click on the extension's 'Details' page.
3. Click on 'Extension Options' to configure it.

## How to use

The extension alternates between two modes: Refractory and Edge.

Refractory mode will display no images at first, but over time will display more and more. To stop the image spam, one must enter Edge mode, wait a specified amount of time, then return to Refractory mode. 

To enter Edge mode, click on the extension's pinned icon (the one next to the URL bar). When the timer expires, click on the icon again to return to Refractory mode.

To remove an image popup from your page, hold your mouse cursor over the image until the animation is complete, then move your cursor off the image.

## Troubleshooting

If no images are being loaded, chances are you put in a bad Booru URL, or bad tags. It's recommended to test this by entering Edge mode, as that will display images more quickly.

