console.log('loaded');

//document.body.style.position = 'relative';

const maxPopup = 100;

const defaultOptions = {
    api: false,
    apicred: "",
    popupdur: "0.5",
    popupfreq: "0.5",
    replacedur: "0.5",
    replacefreq: "0.5",
    tabfreq: "0.5",
    tags: "",
    url: "https://hgoon.booru.org/",
    edge: 15,
    refractory: 15,
    cover: false,
    minscore: 0,
    cspam: false
};

let options = defaultOptions;
let content = document.documentElement;

let imgs;
function updateImages() {
    imgs = [...document.getElementsByTagName('img')];
    imgs = imgs.filter(img => img.clientWidth > 50 && img.clientHeight > 50);
}
updateImages();
setInterval(updateImages, 1000 * 15);

setTimeout(function() {
    const elements = document.querySelectorAll('div', 'body');
    content = document.documentElement;
    let bestHeight = content.clientHeight;
    for(el of elements) {
        if(el.clientHeight > bestHeight) {
            bestHeight = el.clientHeight;
            content = el;
        }
    }
}, 500);

function updateOptions() {
    if(document.hidden)
        return;
    chrome.runtime.sendMessage({ type: 'getOptions' }, o => { options = o; });

}

updateOptions();
setInterval(updateOptions, 5000);

const fg = document.createElement('img');
fg.referrerPolicy = 'no-referrer';
fg.style.position = 'fixed';
fg.style.left = '0px';
fg.style.top = '0px';
fg.style['max-width'] = fg.style['width'] = '100vw';
fg.style['max-height'] = fg.style['height'] = '100vw';
fg.style['z-index'] = 998;
fg.style['object-fit'] = 'cover';
fg.style.opacity = 0.0;
fg.style['pointer-events'] = 'none';
fg.style.display = 'none';
fg.src = '';
fg.popup = true;
document.body.appendChild(fg);

// Foreground

function updateFg() {
    if(options.cover)
        fg.style.opacity = Math.max(0, Math.min(0.25, (options.horny - 120) / 2000));
    else
        fg.style.display = 'none';
}

function makeFg(src) {
    if(!src)
        return;
    fg.src = src;
    fg.style.display = 'block';
    updateFg();
}

function loadFg() {
    if(document.hidden)
        return;
    if(options.cover && Math.random() < options.popupfreq && Math.random() + (options.horny * 0.002) > 1.0) {
        chrome.runtime.sendMessage({ type: 'getImg' }, booruImg => { makeFg(booruImg.src); });
    }
}

chrome.runtime.sendMessage({ type: 'getImg' }, booruImg => { makeFg(booruImg.src); });
setInterval(updateFg, 5000);
setInterval(loadFg, 2000);

// Visibility helpers

/*
function isScrolledIntoView(el) {
    const rect = el.getBoundingClientRect();
    return (rect.top >= -window.innerHeight) && (rect.bottom <= window.innerHeight * 2);
}
*/

function respondToVisibility(element, callback) {
    const thresh = 0.5;
    const observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            callback(entry.intersectionRatio > thresh);
        });
    }, { threshold: thresh });

    observer.observe(element);
}

// Popups

let atMap = {};
const atScale = window.innerWidth * 0.30;

function atKey(x, y) {
    return Math.round(x / atScale) + ',' + Math.round(y / atScale);
}

function hasKey(x, y) {
    return atMap.hasOwnProperty(atKey(x, y));
}

let numPopups = 0;

function makePopup(src, offScreen) {
    if(!src)
        return;
    console.log('making popup', src);
    let zindex = 1000;

    const pop = document.createElement('img');
    pop.referrerPolicy = 'no-referrer';
    pop.style['max-width'] = '50vw';
    pop.style['max-height'] = '50vw';
    pop.style['object-fit'] = 'cover';
    pop.style.filter = '';
    pop.src = src
    pop.popup = true;
    pop.onclick = function() { window.open(src, '_blank').focus(); };

    const popParent = content;

    if(options.cspam && Math.random() * options.horny > 600) {
        pop.style.left = (Math.random() * window.innerWidth * 0.5) + 'px';
        pop.style.top = (Math.random() * window.innerHeight * 0.5) + 'px';
        pop.style.position = 'fixed';
        pop.style['z-index'] = zindex = 1005;

        setTimeout(function() {
            popParent.removeChild(pop);
            --numPopups;
        }, 1500 + 2048000 * Math.pow(options.popupdur, 10));
    }
    else {
        pop.style.position = 'absolute';
        pop.style['z-index'] = zindex = 1000;

        let rx = Math.round(Math.random() * content.offsetWidth * 0.50 + 100) - 50;
        let ry;
        let i = 0;
        let has;
        do {
            ry = Math.round(Math.random() * (content.offsetHeight - window.innerHeight * 0.5));
            ++i;
            if(i > 100)
                return;
            has = false;
            for(let x = -1; x <= 1; ++x) {
            for(let y = -1; y <= 1; ++y) {
                if(hasKey(rx + x*atScale, ry + y*atScale)) {
                    has = true;
                    break;
                }
            }}
        } while(has || (offScreen && (ry > window.scrollY - window.innerHeight * 0.50 - 10 && ry < window.scrollY + window.innerHeight + 10)));

        const at = atKey(rx, ry);
        atMap[at] = true;

        pop.style.left = rx + 'px';
        pop.style.top = ry + 'px';
        console.log('src', src);

        function fade() {
            if(pop.ticked === undefined)
                return;
            pop.ticked -= pop.ticked * 0.1 * (1.0 - options.popupdur * options.popupdur);
            pop.ticked -= 0.1 * (1.0 - options.popupdur * options.popupdur);
            pop.style['outline-style'] = 'double';
            pop.style['outline-color'] = `rgba(255,255,255,${1.0 - pop.ticked / 1000})`;
            pop.style['outline-width'] = pop.ticked + 'px';
            pop.style['box-shadow'] = `0 0 0 200vw rgba(0,0,0,${1.0 - pop.ticked / 1000})`;
            if(pop.ticked > 1)
                setTimeout(fade, 8);
        }

        function del() {
            popParent.removeChild(pop);
            --numPopups;
            delete atMap[at];
        }

        pop.onmouseover = function() {
            if(options.popupdur > 0) {
                pop.ticked = 1000;
                pop.style['z-index'] = zindex + 1;
                fade();
            }
        };

        pop.onmouseleave = function() {
            if(pop.ticked <= 1 || options.popupdur <= 0) {
                del();
            }
            else {
                pop.style['z-index'] = zindex;
                pop.style['outline'] = '';
                pop.style['box-shadow'] = '';
                pop.ticked = undefined;
            }
        };

        let visible = false;

        function step() {
            if(visible)
            {
                if(!document.hidden) {
                    chrome.runtime.sendMessage({ type: 'addHorny', amount: 1.0 });
                }
                setTimeout(step, 500);
            }
        }

        respondToVisibility(pop, v => {
            visible = v;
            step();
        });
    }

    ++numPopups;
    popParent.appendChild(pop);
}

function loadPopup(startup = false) {
    if(document.hidden)
        return;
    const offScreen = !startup || window.innerHeight * 1.5 < content.clientHeight;
    if(numPopups < maxPopup) {
        if(Math.random() < options.popupfreq && Math.random() + ((options.horny - 120)* 0.002) > 1.0) {
            chrome.runtime.sendMessage({ type: 'getImg' }, booruImg => { makePopup(booruImg.src, offScreen); });
        }
    }
}

setInterval(loadPopup, 500);

chrome.runtime.sendMessage({ type: 'getOptions' }, o => {
    if(o.horny > 200)
        for(let i = 0; i < Math.min((o.horny - 200) / 20, 20); ++i)
            loadPopup(true);
});

// Replace image

function replaceImage() {
    if(document.hidden)
        return;
    if(imgs.length === 0)
        return;

    console.log('replace');

    const i = Math.floor(Math.random() * imgs.length);
    const img = imgs[i];

    chrome.runtime.sendMessage({ type: 'getImg' },
        booruImg => {
            if(!booruImg)
                return;
            const booruRatio = booruImg.width / booruImg.height;

            let bestD;
            let bestImg;

            for(let i = 0; i < 20; ++i) {
                let img = imgs[Math.floor(Math.random() * imgs.length)];
                if(img.popup) {
                    continue;
                }

                if(img.clientWidth < 50 || img.clientHeight < 50) {
                    continue;
                }

                let imgRatio = img.naturalWidth / img.naturalHeight;

                if(img.clientWidth < 100 || img.clientHeight < 100)
                    imgRatio *= 2;

                if(!img.hasOwnProperty('orig'))
                    img.orig = img.src;

                if(img.src !== img.orig) {
                    continue;
                }

                const d = Math.abs(imgRatio - booruRatio);
                if(bestD === undefined || d < bestD) {
                    bestD = d;
                    bestImg = img;

                    if(bestD < 0.1)
                        break;
                }
            }

            if(bestImg) {
                const oldSrc = bestImg.src;

                bestImg.src = booruImg.src;
                bestImg.onclick = function() { window.open(booruImg.src, '_blank').focus(); };
                bestImg.style['object-fit'] = 'cover';
                bestImg.style.width = bestImg.clientWidth + 'px';
                bestImg.style.height = bestImg.clientHeight + 'px';

                console.log('did replace');

                respondToVisibility(bestImg, v => {
                    if(v) {
                        if(options.replacedur < 1) {
                            setTimeout(function(){ 
                                if(bestImg.src === booruImg.src)
                                    bestImg.src = bestImg.orig;
                                bestImg.onclick = undefined;
                            }, 500 + 2048000 * Math.pow(options.replacedur, 10));
                        }
                    }
                });
            }
        });
}

function loadReplaceImage() {
    if(document.hidden)
        return;
    if(options.horny > 0 && Math.random() < options.replacefreq && Math.random() + (options.horny * 0.002) > 0.75) {
        replaceImage();
    }
}

setInterval(loadReplaceImage, 500);

