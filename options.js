// TODO
/*
document.getElementById('cum').onclick = function() { 
    chrome.runtime.sendMessage({ type: 'cum' });
};
*/

chrome.runtime.sendMessage({ type: 'getOptions' }, options => {
    for(let key in options) {
        const el = document.getElementById(key);
        if(!el)
            continue;
        el.value = options[key];
        el.checked = !!options[key];
    }
});

document.getElementById('commit').onclick = function() { 
    const data = new FormData(document.getElementById('settings'));
    const value = Object.fromEntries(data.entries());
    value.type = 'setOptions';
    value.tags = value.tags.replace(/\ |\,/, '+');

    if(!value.cspam)
        value.cspam = false;
    if(!value.cover)
        value.cover = false;

    chrome.runtime.sendMessage(value);
    alert('Committed! Changes may take time to appear.');
};

document.getElementById('api').addEventListener('change', function(e){
    document.getElementById('apicred').disabled = !this.checked;
});
